// COMS20001 - Cellular Automaton Farm

#include <platform.h>
#include <xs1.h>
#include <stdio.h>
#include "pgmIO.h"
#include "i2c.h"

#define  IMHT 256                            //height of the input image
#define  IMWD 256                            //width of the input image

#define  N_WORKERS 7                         //number of worker threads

#define  BLHT ((IMHT/N_WORKERS)+5)           //height of the data block each worker uses
#define  BLWD (IMWD/8)                       //width of the data block each worker uses

typedef unsigned char uchar;                 //using uchar as shorthand

const char infname[] = "256x256.pgm";        //path to the input image
char outfname[20];                           //name of the output image

#define LIVE 255                             //cell representation in PGM format
#define DEAD 0

#define REQ_ACT   0                          //request one iteration of acting
#define REQ_PRINT 2                          //request worker to print out data

#define SWITCH_SGREEN 1                      //constants for managing board LEDs
#define SWITCH_BLUE   2
#define SWITCH_GREEN  4
#define SWITCH_RED    8

on tile[0] : port p_scl = XS1_PORT_1E;       //interface ports to orientation
on tile[0] : port p_sda = XS1_PORT_1F;

on tile[0] : in port buttons = XS1_PORT_4E;  //port to access xCore-200 buttons
on tile[0] : out port leds = XS1_PORT_4F;    //port to access xCore-200 LEDs


#define FXOS8700EQ_I2C_ADDR 0x1E             //register addresses for orientation
#define FXOS8700EQ_XYZ_DATA_CFG_REG 0x0E
#define FXOS8700EQ_CTRL_REG_1 0x2A
#define FXOS8700EQ_DR_STATUS 0x0
#define FXOS8700EQ_OUT_X_MSB 0x1
#define FXOS8700EQ_OUT_X_LSB 0x2
#define FXOS8700EQ_OUT_Y_MSB 0x3
#define FXOS8700EQ_OUT_Y_LSB 0x4
#define FXOS8700EQ_OUT_Z_MSB 0x5
#define FXOS8700EQ_OUT_Z_LSB 0x6


/////////////////////////////////////////////////////////////////////////////////////////
//
// Worker interface defining all the relevant functions.
//
/////////////////////////////////////////////////////////////////////////////////////////
interface worker_interface {
   uchar getData (uchar byte, uchar row);
    void sendData (uchar data);
    void setTopGhost(uchar g_top[]);
    void setBotGhost(uchar g_bot[]);
    void getTopEdge(uchar top[]);
    void getBotEdge(uchar bot[]);
    void notify(uchar request, uchar value);
};


/////////////////////////////////////////////////////////////////////////////////////////
//
// Waits for a given amount of time. Used as a delay between some computations.
//
/////////////////////////////////////////////////////////////////////////////////////////
void waitMoment(int time) {
  timer tmr;
  int waitTime;
  tmr :> waitTime;                        //read current timer value
  waitTime += time * 10000000;            //set the waiting time (in milliseconds)
  tmr when timerafter(waitTime) :> void;  //wait until waitTime is reached
}


/////////////////////////////////////////////////////////////////////////////////////////
//
// Reads in a color switch value SWITCH_RED, SWITCH_SGREEN, SWITCH_BLUE or SWITCH_GREEN
// and toggles the corresponding LED by sending it to the showLEDs function.
//
/////////////////////////////////////////////////////////////////////////////////////////
void visualiser(chanend fromBoss, chanend fromDataInStream, chanend toLEDs) {
    int pattern = 0;                      //the current state of all LEDs
    int color;                            //the LED to be switched
    toLEDs <: pattern;
    while (1) {
        select {
          case fromBoss :> color:
              pattern = pattern ^ color;  //flip the bit corresponding to the color
              toLEDs <: pattern;          //send the new pattern to showLEDs
              break;
          case fromDataInStream :> color:
              pattern = pattern ^ color;
              toLEDs <: pattern;
              break;
        }
    }
}


/////////////////////////////////////////////////////////////////////////////////////////
//
// Takes in a pattern and adjusts the LEDs on the board.
//
/////////////////////////////////////////////////////////////////////////////////////////
int showLEDs(out port p, chanend fromVisualiser) {
  int pattern; //1st bit...separate green LED
               //2nd bit...blue LED
               //3rd bit...green LED
               //4th bit...red LED
  while (1) {
    fromVisualiser :> pattern;  //receive new LED patterns from the visualiser
    p <: pattern;               //send the pattern to the LED port
  }
  return 0;
}


/////////////////////////////////////////////////////////////////////////////////////////
//
// Reads input from the board buttons.
//
/////////////////////////////////////////////////////////////////////////////////////////
void buttonListener(in port b, chanend toDataInStream, chanend toSnapListener) {
  int r;
  int readingStarted = 0;
  while (1) {
    b when pinseq(15)  :> r;       //check that no button is pressed
    b when pinsneq(15) :> r;       //check if some buttons are pressed

    if(!readingStarted && r==14) {
        readingStarted = 1;        //mark the beginning of image reading
        toDataInStream <: r;       //send the value of SW1 button input to DataInStream
    }
    if(r==13) {
        toSnapListener <: r;       //send the value of SW2 button input to snapListener
    }
  }
}


/////////////////////////////////////////////////////////////////////////////////////////
//
// Reads in an image from path infname[] to channel c_out. Also manages the timers used
// for certain computations.
//
/////////////////////////////////////////////////////////////////////////////////////////
void DataInStream(const char infname[], chanend c_out, chanend fromButtonListener, chanend toVisualiser) {
  int res, r_started = 0;
  uchar line[ IMWD ];
  fromButtonListener :> r_started; //Check if the reading button has been pressed
  toVisualiser <: SWITCH_GREEN;    //Indicate that reading started by turning on a green LED
  printf( "DataInStream: Start...\n" );

  char inname[20];
  int p;
  for (p = 0; infname[p] != '\0'; ++p)
      inname[p] = infname[p];
  inname[p] = '\0';

  //Open PGM file
  res = _openinpgm( inname, IMWD, IMHT );
  if( res ) {
    printf( "DataInStream: Error openening %s\n.", inname );
    return;
  }

  //Read image line-by-line and send byte by byte to channel c_out
  for( int y = 0; y < IMHT; y++ ) {
    _readinline( line, IMWD );
    for( int x = 0; x < IMWD; x++ ) {
      c_out <: line[ x ];
    }
  }

  //Close PGM image file
  _closeinpgm();
  printf( "DataInStream: Done...\n" );

  //Turn off the green LED
  toVisualiser <: SWITCH_GREEN;

  //Set up a timer for the computations
  timer time;
  uint32_t localTime;
  time :> localTime;
  uint32_t elapsedTime;
  int request;
  while(1) {
      select {
          case c_out :> request:
              if(request == 1) {
                  time :> elapsedTime;
                  c_out <: (elapsedTime - localTime)/1000000;
              }
              else {
                  time :> localTime;
              }
           break;
      }
  }
  return;
}


/////////////////////////////////////////////////////////////////////////////////////////
//
// Changes the world state by one iteration step, depending on which state the world is in.
//
/////////////////////////////////////////////////////////////////////////////////////////
void act(uchar block_size, uchar world[BLWD][BLHT], char state) {
    //According to the state, set the bytes of the two auxiliary lines to 0
    for(int byte = 0; byte < BLWD; byte++) {
        world[byte][(1-state)*block_size] = 0x00;
        world[byte][(1-state)*block_size+1] = 0x00;
    }
    int isData = 1;
    //Compute the line where the computation starts from, according to the state
    int y = (1-state)*(block_size -1) + state*2;
    //Act on every bit of every byte inside the worker's block
    while(isData) {
        for(int x = 0; x < BLWD; x++) {
          uchar byte = world[x][y];
          for( int bit = 0; bit < 8; bit++) {
            uchar isAlive, nAlive = 0;
            uchar l, r, t, b;

            //Look to the left bit
            l = (bit == 7 ? 0 : bit+1);
            //Look to the top bit
            t = bit;
            //Look to the right bit
            r = (bit == 0 ? 7 : bit-1);
            //Look to the bottom bit
            b = bit;

            //By default only look inside the current byte
            int byte_index_left  = x;
            int byte_index_right = x;

            //At the first or the last bit in the byte, adjust indexes accordingly
            if(l == 0) {
                byte_index_left = x > 0 ? x-1 : BLWD-1;
            }
            if(r == 7) {
                byte_index_right = x < BLWD-1 ? x+1 : 0;
            }

            //Depending on the current state, set indexes of top and bottom row
            int top_row = y == state*2 ? block_size + 2 : y-1;
            int bot_row = y == block_size - 1 + state*2 ? block_size + 3 : y+1;

            //Check the top neighbour
            nAlive += (world[x][top_row] & (1 << t)) >> t;
            //Check the bottom neighbour
            nAlive += (world[x][bot_row] & (1 << b)) >> b;
            //Check the left neighbour
            nAlive += (world[byte_index_left][y] & (1 << l)) >> l;
            //Check the right neighbour
            nAlive += (world[byte_index_right][y] & (1 << r)) >> r;
            //Check the left corner neighbour
            nAlive += (world[byte_index_left][top_row] & (1 << l)) >> l;
            //Check the right corner neighbour
            nAlive += (world[byte_index_right][top_row] & (1 << r)) >> r;
            //Check the left bottom neighbour
            nAlive += (world[byte_index_left][bot_row] & (1 << l)) >> l;
            //Check the right bottom neighbour
            nAlive += (world[byte_index_right][bot_row] & (1 << r)) >> r;

            //Apply the 'Laws of Physics'
            isAlive = (byte & (1 << bit)) >> bit;
            if(!isAlive) {
              if(nAlive == 3)
                isAlive = 1;
            }
            else {
              if(nAlive < 2 || nAlive > 3)
                isAlive = 0;
            }
            //Change the world according to its current state
            world[x][state ? y-2 : y+2] ^= (-isAlive ^ world[x][state ? y-2 : y+2]) & (1 << bit);
          }
        }
        y = state ? y+1 : y-1;
        if(!state && y < 0) isData = 0;
        else if (state && y > block_size + 1) isData = 0;
    }
}


/////////////////////////////////////////////////////////////////////////////////////////
//
// Worker thread that acts continuously, unless requested to take a snapshot.
//
/////////////////////////////////////////////////////////////////////////////////////////
void worker(server interface worker_interface i, int block_size) {
    uchar data[BLWD][BLHT];
    int x = 0, y = 0;
    char state = 0;
    uchar shouldWork = 0;
    while (1) {
      select {
          //Get data to work on
          case i.sendData(uchar value):
              data[x][y] = value;
              x++;
              if(x == BLWD) {
                  x = 0;
                  y++;
              }
              break;
          //Send current data if a snapshot is requested
          case i.getData(uchar l_byte, uchar row) -> uchar byte:
              byte = data[l_byte][row+state*2];
              break;
          //Fill in the top ghost row with corresponding data
          case i.setTopGhost(uchar g_top[]):
              for(int i = 0; i < BLWD; i++)
                  data[i][block_size+2] = g_top[i];
              break;
          //Fill in the bot ghost row with corresponding data
          case i.setBotGhost(uchar g_bot[]):
              for(int i = 0; i < BLWD; i++)
                  data[i][block_size+3] = g_bot[i];
              break;
          //Get the last line of data, that is used by bottom neighbour worker
          case i.getBotEdge(uchar bot[]):
              for(int i = 0; i < BLWD; i++)
                  bot[i] = data[i][block_size-1+state*2];
              break;
          //Get the last line of data, that is used by top neighbour worker
          case i.getTopEdge(uchar top[]):
              for(int i = 0; i < BLWD; i++)
                 top[i] = data[i][state*2];
              break;
          //Notify the worker to either start acting or print his data
          case i.notify(uchar request, uchar value):
              switch(request) {
                  case REQ_PRINT : {
                      for (int b_row=state*2; b_row < block_size+state*2; b_row++) {
                          printf("%d ",b_row);
                          for(int n_byte=0; n_byte < BLWD; n_byte++) {
                              uchar byte = data[n_byte][b_row];
                              for (int bit = 7; bit >= 0; bit--) {
                                  char c = ((byte & (1 << bit)) >> bit) ? '*' : ' ';
                                  printf("%c", c);
                              }
                          }
                          printf("\n");
                      }
                      printf("-------------------\n");
                      break;
                  }
                  case REQ_ACT : {
                      shouldWork = 1;
                      break;
                  }
              }
              break;
      }
      //Act for one iteration, change the state and wait for instructions
      if(shouldWork){
          act(block_size, data, state);
          shouldWork = 0;
          state = 1 - state;
      }
    }
}

/////////////////////////////////////////////////////////////////////////////////////////
//
// The Boss thread that manages all the worker threads.
//
/////////////////////////////////////////////////////////////////////////////////////////
void boss(chanend c_in, chanend c_out, chanend toVisualiser,
          chanend toSnap, chanend fromAcc, const int nWorkers, const int block_sizes[], client interface worker_interface w[]) {
    int isTilted;     //trigger from accelerometer
    uchar pixel;      //pixel read in from the image

    //------Assign workload------//
    for(int worker = 0; worker < nWorkers; worker ++) {
        for(int line = 0; line < block_sizes[worker]; line++) {
            for( int x = 0; x < BLWD; x++ ) {
                uchar byte = 0;
                for(int bit = 0; bit < 8; bit++){   //go through each pixel per line
                    c_in :> pixel;                  //read the pixel value into a byte
                    if(pixel == LIVE) byte = byte | 1;
                    if(bit < 7) byte = byte << 1;
                }
                w[worker].sendData(byte);           //send each byte to the worker
            }
        }
    }

    int iteration = 1;    //current iteration number
    int isRedLightOn = 0;
    int check = 1;
    while(1) {
      //------Manage snapshots------//
      check = 1;
      toSnap <: check; //check if a snapshot has been requested
      toSnap :> check; //receive the answer
      //Export the first few images
      if(check || (iteration <= 1 && iteration > 1)) {
          //Toggle the blue LED at the start of the snapshot
          toVisualiser <: SWITCH_BLUE;
          //Save world to image
          printf( "\nWriting world state to image...\n" );
          c_out <: iteration;
          //Gather data from all the workers
          for(int worker = 0; worker < nWorkers; worker ++) {
              for(int line = 0; line < block_sizes[worker]; line++) {
                  for(int x = 0; x < BLWD; x++) {
                      uchar byte;
                      byte = w[worker].getData(x, line);
                      for(int bit = 7; bit >= 0; bit--) {
                          c_out <: (byte & 0x80) > 0 ? (uchar)LIVE : (uchar)DEAD;
                          byte = byte << 1;
                      }
                  }
              }
          }
          //Toggle the blue LED at the end of the snapshot
          toVisualiser <: SWITCH_BLUE;
          //Notify of completed snapshot
          if(check) toSnap <: 0;
      }

      //------Manage pausing--------//
      isTilted = 0;
      do {
          fromAcc <: 1;                        //Ask if the board is tilted
          fromAcc :> isTilted;                 //Get the answer
          if(isTilted && !isRedLightOn){       //Once tilted, toggle the red LED
              isRedLightOn = 1;
              toVisualiser <: SWITCH_RED;
              // Print number of currently alive cells
              long alive = 0;
              for(int worker = 0; worker < nWorkers; worker ++) {
                  for(int line = 0; line < block_sizes[worker]; line++) {
                      for(int x = 0; x < BLWD; x++) {
                          uchar byte;
                          byte = w[worker].getData(x, line);
                          for(int bit = 7; bit >= 0; bit--) {
                              alive += (byte & 0x80) > 0 ? 1 : 0;
                              byte = byte << 1;
                          }
                      }
                  }
              }
              printf("There are %ld cells alive at iteration %d\n", alive, iteration);
          }
          else if (!isTilted && isRedLightOn){ //Resume computation and toggle LED
              isRedLightOn = 0;
              toVisualiser <: SWITCH_RED;
          }

      } while (isTilted == 1);

      //------Manage iterations------//
      toVisualiser <: SWITCH_SGREEN;
      //Update the ghost rows
      uchar row[BLWD];
      for(int i = 0; i<nWorkers; i++) {
          //Get bottom ghost row from the bottom neighbour worker
          w[(i+1)%nWorkers].getTopEdge(row);
          w[i].setBotGhost(row);
          //Get top ghost row from the top neighbour worker
          if (i > 0) w[i-1].getBotEdge(row);
          else w[nWorkers-1].getBotEdge(row);
          w[i].setTopGhost(row);
      }

      //------Manage acting on the data------//
      //Notify the workers to act for one iteration
      for(int i = 0; i<nWorkers; i++)
          w[i].notify(REQ_ACT, 0);
      int noOfIterations = 100;
      uint32_t answer;
      //Every noOfIterations times, print out the time taken
      if(iteration % noOfIterations == 0) {
          c_in <: 1;
          c_in :> answer;
          c_in <: 0;
          printf("-----%d ITERATIONS TOOK %d -------\n",noOfIterations, answer);
          /*for(int i = 0; i<nWorkers; i++)
              w[i].notify(REQ_PRINT, 0);
          waitMoment(1);*/
      }
      iteration++;
      //Switch the separate green LED on
      toVisualiser <: SWITCH_SGREEN;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////
//
// Distributes the image among all the worker threads.
//
/////////////////////////////////////////////////////////////////////////////////////////
void distributor(chanend c_in, chanend c_out, chanend toVisualiser, chanend toSnap, chanend orientation) {
  //Trigger from accelerometer
  int isTilted;

  //Starting up and wait for tilting of the xCore-200 Explorer
  printf( "ProcessImage: Start, size = %dx%d\n", IMHT, IMWD );
  printf( "Tilt board to begin...\n" );
  do {
      orientation <: 1;
      orientation :> isTilted;
  } while (isTilted != 1);

  //Utilise n workers for farming
  //List containing the number of rows each worker will be assigned
  int block_sizes[N_WORKERS];

  //Calculate block sizes
  int d = IMHT / N_WORKERS;
  int r = IMHT % N_WORKERS;

  //Every worker gets an almost equal number of lines
  if(r==0)
      for(int i = 0; i < N_WORKERS; i++)
          block_sizes[i] = d;
  else {
      for(int i = 0; i < N_WORKERS; i++)
          if(i<r) block_sizes[i] = d+1;
          else block_sizes[i] = d;
  }
  printf( "Press button SW1 to start the Game of Life...\n" );

  //------Initialise the boss and the worker threads in parallel------//
  interface worker_interface w[N_WORKERS];
  par {
      boss(c_in, c_out, toVisualiser, toSnap, orientation, N_WORKERS, block_sizes, w);
      par (size_t i=0; i<N_WORKERS; i++) {
          worker(w[i], block_sizes[i]);
      }
  }
}

/////////////////////////////////////////////////////////////////////////////////////////
//
// Writes pixel stream from channel c_in to a PGM image file.
//
/////////////////////////////////////////////////////////////////////////////////////////
void DataOutStream(char outfname[], chanend c_in) {
  int res;
  int flag;
  int num = 0;
  uchar line[IMWD];
  while(1) {
      select {
          //Wait until a snapshot has been requested
          case c_in :> flag:
          //Open PGM file
          printf( "DataOutStream: Start...\n" );

          //Magically name each output file differently to avoid overwriting
          char s2[20], pgm[5]=".pgm", p;
          for(p = 0; infname[p] != '.'; ++p) {
              outfname[p] = infname[p];
          }
          outfname[p] = '_';
          p++;
          outfname[p] = num +'0';
          p++;
          num++;
          for(int x = 0; pgm[x] != '\0'; ++x) {
              outfname[p] = pgm[x];
              p++;
          }
          outfname[p] = '\0';
          res = _openoutpgm( outfname, IMWD, IMHT );
          if( res ) {
            printf( "DataOutStream: Error opening %s\n.", outfname );
            return;
          }

          //Compile each line of the image and write the image line-by-line
          for(int y = 0; y < IMHT; y++) {
            for(int x = 0; x < IMWD; x++) {
              c_in :> line[x];
            }
            _writeoutline(line, IMWD);
          }
          //Close the PGM image
          _closeoutpgm();
          printf("DataOutStream: Done...\n");
          break;
      }
  }
  return;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
// Manage requests for snapshots.
//
/////////////////////////////////////////////////////////////////////////////////////////
void snapshotListener(chanend fromButtonListener, chanend fromBoss) {
    int snapRequested = 0;
    int request = 0;
    int check = 0;
    while(1) {
        select {
           case fromBoss :> check:
                fromBoss <: snapRequested;     //inform if there was a request
                if(snapRequested) {
                    fromBoss :> snapRequested; //reset snap request
                }
                break;
           case !snapRequested => fromButtonListener :> request:
                snapRequested = 1;
                break;
        }
    }
}


/////////////////////////////////////////////////////////////////////////////////////////
//
// Initialise and  read orientation, send first tilt event to channel.
//
/////////////////////////////////////////////////////////////////////////////////////////
void orientation(client interface i2c_master_if i2c, chanend toBoss) {
  i2c_regop_res_t result;
  char status_data = 0;
  int tilted = 0;

  //Configure FXOS8700EQ
  result = i2c.write_reg(FXOS8700EQ_I2C_ADDR, FXOS8700EQ_XYZ_DATA_CFG_REG, 0x01);
  if (result != I2C_REGOP_SUCCESS) {
    printf("I2C write reg failed\n");
  }

  //Enable FXOS8700EQ
  result = i2c.write_reg(FXOS8700EQ_I2C_ADDR, FXOS8700EQ_CTRL_REG_1, 0x01);
  if (result != I2C_REGOP_SUCCESS) {
    printf("I2C write reg failed\n");
  }

  //Probe the orientation x-axis forever
  while (1) {
    //check until new orientation data is available
    do {
      status_data = i2c.read_reg(FXOS8700EQ_I2C_ADDR, FXOS8700EQ_DR_STATUS, result);
    } while (!status_data & 0x08);

    //get new x-axis tilt value
    int x = read_acceleration(i2c, FXOS8700EQ_OUT_X_MSB);
    int request = 0;
    select {
        case toBoss :> request:
            //send signal to distributor with current value
            tilted = x > 30 ? 1 : 0;
            toBoss <: tilted;
            break;
    }
  }
}

/////////////////////////////////////////////////////////////////////////////////////////
//
// Orchestrate the concurrent system and start up all threads.
//
/////////////////////////////////////////////////////////////////////////////////////////
int main(void) {

i2c_master_if i2c[1];                                                        //interface to orientation

chan c_inIO, c_outIO, c_control, b_control, v_control,
     d_control, din_control, snapListener[2];                                //channel definitions

par {
    on tile[0]: buttonListener(buttons, b_control, snapListener[0]);                     //thread to get input from buttons
    on tile[0]: snapshotListener(snapListener[0], snapListener[1]);                      //thread to manage snapshots
    on tile[0]: showLEDs(leds, v_control);                                               //thread to control LEDs
    on tile[0]: visualiser(d_control, din_control, v_control);                           //thread to control visualiser
    on tile[0]: i2c_master(i2c, 1, p_scl, p_sda, 10);                                    //server thread providing orientation data
    on tile[0]: orientation(i2c[0], c_control);                                          //client thread reading orientation data
    on tile[0]: DataInStream(infname, c_inIO, b_control, din_control);                   //thread to read in a PGM image
    on tile[0]: DataOutStream(outfname, c_outIO);                                        //thread to write out a PGM image
    on tile[1]: distributor(c_inIO, c_outIO, d_control, snapListener[1], c_control);     //thread to coordinate work on image
  }
  return 0;
}
